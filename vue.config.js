module.exports = {
  chainWebpack: config => {
    config.plugin('html').tap(args => {
      args[0].title = 'Incident Dashboard'
      return args
    })
  },
}
