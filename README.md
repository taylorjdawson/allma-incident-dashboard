# Incident Dashboard
SPA site created for an interview wth Allma. View live demo [here](https://incident-dashboard-one.vercel.app)!

![Image of Dashboard](src/assets/incident-dashboard.png)


## Docker Setup

**Build image**
```
docker build . -t incident-dashboard
```

**Run image**
```
docker run -it -p 80:80 --rm incident-dashboard
```

Then navigate to `http://localhost`


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```
