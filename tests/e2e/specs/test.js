import { incidents } from '../../../src/assets/incidents.json'

const TOTAL_INCIDENTS = incidents.length
const OPEN_INCIDENTS = incidents.reduce(
  (total, incident) => total + (incident.incidentStatusId !== 'RESOLVED'),
  0
)
const RESOLVED_INCIDENTS = incidents.length - OPEN_INCIDENTS

// The Id of an open incident
const OPEN_INCIDENT_ID = 35

// The Id of a resolved incident
const RESOLVED_INCIDENT_ID = 33

/**
 * Index incidents by id for fast lookups. Uses an IIFE so that we call the
 * method once and assign its result.
 */
const indexedIncidents = (() => {
  const obj = {}
  for (const incident of incidents) {
    obj[incident.id] = incident
  }
  return obj
})()

/**
 * Gets the incident commander of the incident.
 * @param id - The id of the incident of which to retrieve its commander
 * @return {string} The name of the incident commander or 'Unassigned' if no
 *  user with role.id === 1 found.
 */
const getIncidentCommander = id => {
  let incidentCommander = 'Unassigned'
  const participant = indexedIncidents[id].participants.filter(
    participant =>
      participant.user && participant.role && participant.role.id === 1
  )[0]
  if (participant) {
    incidentCommander = participant.user.realName
  }
  return incidentCommander
}

/**
 * Get the severity level of the incident.
 * @param id - The id of the incident of which to retrieve its severity
 * @return {number|string} - the severity level or 'unknown'
 */
const getIncidentSeverity = id =>
  indexedIncidents[id].severity
    ? indexedIncidents[id].severity.sortOrder
    : 'unknown'

describe('Test incident dashboard', () => {
  /**
   * Visits the page before all tests
   */
  before(() => {
    cy.log('Visiting incident dashboard')
    cy.visit('/')
  })

  it('Should load the page with data', () => {
    // Asserts that the main app is visible
    cy.get('#app').should('be.visible')

    // Checks that the number of incidents displayed matches the total number
    // of incidents within our json file
    cy.get('.App--incident').should('have.length', TOTAL_INCIDENTS)
  })

  it('Should show 6 open incidents', () => {
    cy.get('.IncidentCard--status-open').should('have.length', OPEN_INCIDENTS)
  })

  it('Should show correct incident labels', () => {
    cy.get('.App--incident').each($el => {
      const id = $el.data('id')

      // Assert that the incident name is correct
      cy.wrap($el)
        .find('.IncidentCard--title')
        .contains(indexedIncidents[id].name)

      // Assert that the status label is visible and matches the incident type
      if ($el.hasClass('IncidentCard--status-open')) {
        cy.wrap($el).contains('open', { matchCase: false })
      } else {
        cy.wrap($el).contains('resolved', { matchCase: false })
      }

      // Assert that the incident has the correct incident commander listed
      const commander = getIncidentCommander(id)
      cy.wrap($el)
        .find('.IncidentCard--user-name')
        .contains(commander)

      // Assert that the incident has the right severity level
      const severity = getIncidentSeverity(id)
      cy.wrap($el)
        .find('.IncidentCard--severity')
        .contains(severity)
    })
  })

  it('Should search incidents', () => {
    const QUERY = 'cheetos'
    cy.get('.Search--field-input').type(QUERY)

    // Assert that there is only 1 search result
    cy.get('.App--incident').should('have.length', 1)

    // Assert that the search result contains our query
    cy.get('.App--incident .IncidentCard--title').contains(QUERY)

    // Cleanup clear the query
    cy.get('.Search--field-input').clear()

    // wait for transition to finish before moving on
    cy.wait(800)
  })

  it('Should show no results if incident is not found', () => {
    cy.get('.Search--field-input').type('non existent incident name')

    // Assert that there are no incidents
    cy.get('.App--incidents')
      .children()
      .should('have.length', 0)

    // Assert no results message is visible
    cy.get('.App--no-results').should('be.visible')

    // Cleanup clear the query
    cy.get('.Search--field-input').clear()

    // wait for transition to finish before moving on
    cy.wait(800)
  })

  it('Should filter incidents', () => {
    // Open filter menu
    cy.get('.Search--selected-filter').click()

    // Select resolved filter
    cy.get('.Search--filters-menu .Search--filter[for=resolved]').click()

    // Wait for transition to finish before moving on
    cy.wait(800)

    // Assert only resolved incidents are shown
    cy.get('.IncidentCard--status').each($el => {
      cy.wrap($el).contains('resolved', { matchCase: false })
    })
  })

  it('Should only search filtered incidents', () => {
    // Open filter menu
    cy.get('.Search--selected-filter').click()

    // Select resolved filter
    cy.get('.Search--filters-menu .Search--filter[for=resolved]').click()

    // Wait for transition to finish before moving on
    cy.wait(800)

    cy.get('.Search--field-input').type('cheetos')

    // Assert that there are no incidents
    cy.get('.App--incidents')
      .children()
      .should('have.length', 0)

    // Assert no results message is visible
    cy.get('.App--no-results').should('be.visible')

    // Cleanup clear the query
    cy.get('.Search--field-input').clear()

    // Wait for transition to finish before moving on
    cy.wait(800)
  })
})
