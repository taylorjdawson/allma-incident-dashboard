import {
  Incident,
  IncidentJSON,
  ParticipantJSON,
  Stat,
  Status,
  UserJSON,
} from '@/types'
import FlexSearch from 'flexsearch'

/**
 * Converts a given time (in seconds) to day, hour, minutes, seconds format.
 * @param time - the time in seconds
 * @return An object containing the time values and method to convert these
 * values into a readable string format.
 */
export const formatTime = (time: number) => {
  let _time = time

  const days = Math.floor(_time / 86400)
  _time -= days * 86400

  const hours = Math.floor(_time / 3600) % 24
  _time -= hours * 3600

  const minutes = Math.floor(_time / 60) % 60
  _time -= minutes * 60

  const seconds = Math.round(_time % 60)
  return {
    days,
    hours,
    minutes,
    seconds,
    toString() {
      return `${days}d ${hours}h ${minutes}m ${seconds}s`
    },
  }
}

/**
 * Checks whether a date is within a given number of days.
 * @param date - the date to check
 * @param days - the number of days within the given date
 * @return True if the date is within the given number of days. False otherwise.
 */
export const isWithinDays = (date: Date, days = 30): boolean => {
  return new Date(Date.now() - days * 24 * 60 * 60 * 1000) < date
}

/**
 * Calculates statistics about the collection of incidents.
 * @param incidents - The incidents for which to calculate stats
 */
export const calculateStats = (incidents: Incident[]): Stat[] => {
  // The total number of open (incidentStatusId !== RESOLVED)
  let openIncidents = 0

  // The total number of incidents created within the last 30 days
  let last30Days = 0

  // The mean duration for all incidents that have a RESOLVED state
  let avgResolution = 0

  for (const incident of incidents) {
    if (incident.status === 'open') {
      openIncidents += 1
    } else {
      avgResolution += incident.duration
    }
    last30Days += incident.createdOn && isWithinDays(incident.createdOn) ? 1 : 0
  }
  avgResolution /= incidents.length - openIncidents
  return [
    {
      value: openIncidents,
      label: 'open incidents',
    },
    {
      value: last30Days,
      label: 'last 30 days',
    },
    {
      value: formatTime(avgResolution).toString(),
      label: 'avg resolution time',
    },
  ]
}

/**
 * Gets the incident commander from the array of participants.
 * @param participants - the participants of an incident
 * @return the incident commander or null if there is none
 */
const deserializeUser = (
  participants?: ParticipantJSON[] | null
): UserJSON | null => {
  let user = null
  if (participants) {
    for (const participant of participants) {
      if (participant?.role?.id === 1) {
        user = participant?.user
      }
    }
  }
  return user
}

/**
 * Takes in the raw incident data and massages it for presentation.
 * @param incidents - the raw incident data that get deserialized
 * @return the deserialized incident data
 */
export const deserializeIncidents = (incidents: IncidentJSON[]): Incident[] => {
  const deserializedIncidents = []
  for (const incident of incidents) {
    deserializedIncidents.push({
      id: incident.id,
      name: incident.name,
      status: (incident.incidentStatusId.toLowerCase() !== 'resolved'
        ? 'open'
        : 'resolved') as Status,
      severity: incident?.severity
        ? {
            level: incident?.severity?.sortOrder,
            description: incident?.severity?.description,
          }
        : null,
      user: deserializeUser(incident.participants),
      channel: {
        name: incident?.channelName,
        link: `slack://channel?team=${incident?.workspace.teamId}&id=${incident?.channelId}`,
      },
      createdOn: incident?.createdOn ? new Date(incident.createdOn) : null,
      duration: incident?.duration,
    })
  }
  return deserializedIncidents
}
/**
 * Indexes incidents for searching. An index is created on the 'name' and
 * 'summary' field.
 * @param incidents - the list of incidents
 */
export const indexIncidents = (incidents: Incident[]) => {
  return FlexSearch.create({
    doc: {
      id: 'id',
      field: ['name', 'user'],
    },
  }).add(incidents)
}
