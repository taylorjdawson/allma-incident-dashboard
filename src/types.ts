/**
 * Types for json data before they is deserialized.
 */

export interface IncidentJSON {
  id: number
  name: string
  incidentStatusId: string
  impact?: null
  summary?: null
  duration: number
  resolution?: null
  channelId: string
  channelName: string
  channelPrivate: boolean
  identifiedOn?: string | null
  mitigatedOn?: string | null
  resolvedOn?: string | null
  createdOn: string | null
  workspace: WorkspaceJSON
  severity?: SeverityJSON | null
  participants?: ParticipantJSON[] | null
}

interface SeverityJSON {
  id: number
  name: string
  emoji: string
  description: string
  sortOrder: number
}

interface WorkspaceJSON {
  id: number
  name: string
  teamId: string
  appId: string
}

export interface ParticipantJSON {
  user: UserJSON | null
  role: RoleJSON | null
}

export interface UserJSON {
  id: string
  realName: string
  avatarUrl: string
  chatUserId: string
}

interface RoleJSON {
  id: number
  name: string
  description: string
}

// Type for deserialized incident data

/**
 * The status of an incident. If incidentStatusId !== RESOLVED then the status
 * is 'open' otherwise it is 'resolved'.
 */
export type Status = 'open' | 'resolved'

/**
 * The Severity of the incident's.
 */
export interface Severity {
  // The level of severity, 1 being the highest
  level: number

  // The description of the severity level
  description: string
}

/**
 * The Slack Channel associated with the incident
 */
export interface SlackChannel {
  // The name of the slack channel
  name: string
  // The slack channel deep link
  link: string
}

/**
 * A de-serialized incident.
 */
export interface Incident {
  id: number
  name: string
  status: Status
  severity: Severity | null
  user: UserJSON | null
  channel: SlackChannel
  createdOn: Date | null
  duration: number
}

/**
 * Stats about the collection of incidents such as the number of open incidents.
 */
export interface Stat {
  value: number | string
  label: string
}
